import scala.language.postfixOps

case class Person(fname: String, lname: String){
  var email: String = ""
}

@main
def main(): Unit = {
  val person1 = new Person("Jon", "Doe")
  person1.email = "person1@gmail.com"
  val person2 = new Person("Jon", "Doe")
  person2.email = "person2@gmail.com"

  println(person1==person2)

}
